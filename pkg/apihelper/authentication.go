package apihelper

import "net/http"

func authentic(w http.ResponseWriter, r *http.Request) bool {
	t := r.Header.Get("Authentication")
	if t == "" {
		MakeResponse(w, "Token Required", http.StatusUnauthorized)
		return false
	} else if !valid(t) {
		MakeResponse(w, "Invalid Token", http.StatusUnauthorized)
		return false
	}
	return true
}

func valid(token string) bool {
	if token == "MyAwesomeToken" {
		return true
	}
	return false
}
