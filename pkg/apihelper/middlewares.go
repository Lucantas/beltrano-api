package apihelper

import (
	"net/http"
)

// Middleware is responsible to handle middlewares om the api
// such as authentication, preflight handlers, and also methods
func Middleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		preflight(w)
		if r.Method == "OPTIONS" {
			return
		}
		if authentic(w, r) {
			next.ServeHTTP(w, r)
		}
	})
}

func preflight(w http.ResponseWriter) {
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Methods", "OPTIONS")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type")
}
