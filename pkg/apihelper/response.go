package apihelper

import (
	"encoding/json"
	"net/http"
)

//response is a basic struct cotaining a name and statuscode to an error
type response struct {
	Message string
}

func MakeResponse(w http.ResponseWriter, n string, status int) {
	w.WriteHeader(status)
	json.NewEncoder(w).Encode(response{Message: n})
}

func HandleMethods(w http.ResponseWriter, reqMethod string, allow ...string) {
}
