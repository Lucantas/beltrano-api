package erp

import (
	"beltrano-api/pkg/apihelper"
	"beltrano-api/pkg/relation"
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

type empOrdersJSON struct {
	EmployeeID string `json:"employeeId"`
}

func Employees(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Methods", "GET")
	switch r.Method {
	case "OPTIONS":
		return
	case "PUT":
		//
	case "GET":
		employees(w)
	case "POST":
		apihelper.MakeResponse(w, "Method Not Allowed", http.StatusMethodNotAllowed)
	}
}

func employees(w http.ResponseWriter) {
	se := relation.ShowEmployees()
	e := json.NewEncoder(w)
	e.Encode(se)
}

func EmployeeOrders(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Methods", "GET, PUT")
	id, err := strconv.Atoi(mux.Vars(r)["id"])
	if err != nil {
		apihelper.MakeResponse(w, "Invalid Id", http.StatusUnprocessableEntity)
	}
	switch r.Method {
	case "OPTIONS":
		return
	case "GET":
		employeeOrders(w, id)
	case "POST", "PUT":
		apihelper.MakeResponse(w, "Method Not Allowed", http.StatusMethodNotAllowed)
	}
}

func employeeOrders(w http.ResponseWriter, employeeID int) {
	so := relation.EmployeeOrders(employeeID)
	e := json.NewEncoder(w)
	e.Encode(so)
}
