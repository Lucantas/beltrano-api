package erp

import (
	"beltrano-api/pkg/apihelper"
	"beltrano-api/pkg/relation"
	"encoding/json"
	"net/http"
)

func Orders(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	switch r.Method {
	case "OPTIONS":
		return
	case "PUT":
		//
	case "GET":
		orders(w)
	case "POST":
		apihelper.MakeResponse(w, "Method Not Allowed", http.StatusMethodNotAllowed)
	}
}

func orders(w http.ResponseWriter) {
	se := relation.ShowOrders()
	e := json.NewEncoder(w)
	e.Encode(se)
}
