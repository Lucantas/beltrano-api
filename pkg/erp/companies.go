package erp

import (
	"beltrano-api/pkg/apihelper"
	"beltrano-api/pkg/relation"
	"encoding/json"
	"log"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

func Companies(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Methods", "GET")
	switch r.Method {
	case "OPTIONS":
		return
	case "PUT":
		//
	case "GET":
		companies(w)
	case "POST":
		apihelper.MakeResponse(w, "Method Not Allowed", http.StatusMethodNotAllowed)
	}
}

func companies(w http.ResponseWriter) {
	sc := relation.ShowCompanies()
	e := json.NewEncoder(w)
	if len(*sc) < 1 {
		e.Encode([]relation.ClientePj{})
		return
	}
	e.Encode(sc)
}

func CompaniesFromCourse(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Methods", "GET")
	course := courseJSON{}
	if err := json.NewDecoder(r.Body).Decode(&course); err != nil {
		apihelper.MakeResponse(w, "Invalid inputs found", http.StatusUnprocessableEntity)
	}
	switch r.Method {
	case "OPTIONS":
		return
	case "GET":
	case "PUT":
		apihelper.MakeResponse(w, "Method Not Allowed", http.StatusMethodNotAllowed)
	case "POST":
		companiesFromCourse(w, course.Name)
	}
}

func companiesFromCourse(w http.ResponseWriter, course string) {
	sc := relation.CompaniesFromCourse(course)
	e := json.NewEncoder(w)
	if len(*sc) < 1 {
		e.Encode([]relation.ClientePj{})
		return
	}
	e.Encode(sc)
}

func CompaniesFromClass(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Methods", "GET")
	class := classJSON{}
	if err := json.NewDecoder(r.Body).Decode(&class); err != nil {
		apihelper.MakeResponse(w, "Invalid inputs found", http.StatusUnprocessableEntity)
	}
	log.Println(class)
	switch r.Method {
	case "OPTIONS":
		return
	case "GET":
	case "PUT":
		apihelper.MakeResponse(w, "Method Not Allowed", http.StatusMethodNotAllowed)
	case "POST":
		companiesFromClass(w, class.ClassID)
	}
}

func companiesFromClass(w http.ResponseWriter, class int) {
	sc := relation.CompaniesFromClass(class)
	e := json.NewEncoder(w)
	if len(*sc) < 1 {
		e.Encode([]relation.ClientePj{})
		return
	}
	e.Encode(sc)
}

func CoursesFromCompany(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Methods", "GET")
	var i int
	if int, err := strconv.Atoi(mux.Vars(r)["id"]); err != nil {
		apihelper.MakeResponse(w, "Id must be an integer", http.StatusBadRequest)
	} else {
		i = int
	}
	switch r.Method {
	case "POST":
	case "PUT":
		apihelper.MakeResponse(w, "Method Not Allowed", http.StatusMethodNotAllowed)
	case "GET":
		courseByCompany(w, i)
	}
}

func courseByCompany(w http.ResponseWriter, ID int) {
	c := relation.CoursesByCompany(ID)
	e := json.NewEncoder(w)
	if len(*c) < 1 {
		e.Encode(relation.ClientePj{})
	} else {
		e.Encode(c)
	}
}
