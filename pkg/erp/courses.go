package erp

import (
	"beltrano-api/pkg/apihelper"
	"beltrano-api/pkg/relation"
	"encoding/json"
	"net/http"
)

func Courses(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	switch r.Method {
	case "OPTIONS":
		return
	case "PUT":
		//
	case "GET":
		courses(w)
	case "POST":
		apihelper.MakeResponse(w, "Method Not Allowed", http.StatusMethodNotAllowed)
	}
}

func courses(w http.ResponseWriter) {
	se := relation.ShowCourses()
	e := json.NewEncoder(w)
	e.Encode(se)
}

func Classes(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	switch r.Method {
	case "OPTIONS":
		return
	case "PUT":
		//
	case "GET":
		classes(w)
	case "POST":
		apihelper.MakeResponse(w, "Method Not Allowed", http.StatusMethodNotAllowed)
	}
}

func classes(w http.ResponseWriter) {
	se := relation.ShowClasses()
	e := json.NewEncoder(w)
	e.Encode(se)
}
