package erp

import (
	"beltrano-api/pkg/apihelper"
	"beltrano-api/pkg/relation"
	"encoding/json"
	"log"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

type courseJSON struct {
	Name string `json:"name"`
}

type classJSON struct {
	ClassID int `json:"classId"`
}

// Clients is a request who accepts the GET method, in order to return
// a list with all the clients in the database
func Clients(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Methods", "GET")
	switch r.Method {
	case "OPTIONS":
		return
	case "POST":
	case "PUT":
		apihelper.MakeResponse(w, "Method Not Allowed", http.StatusMethodNotAllowed)
	case "GET":
		clients(w)
	}
}

func CoursesFromClient(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Methods", "GET")
	var i int
	if int, err := strconv.Atoi(mux.Vars(r)["id"]); err != nil {
		apihelper.MakeResponse(w, "Id must be an integer", http.StatusBadRequest)
	} else {
		i = int
	}
	switch r.Method {
	case "POST":
	case "PUT":
		apihelper.MakeResponse(w, "Method Not Allowed", http.StatusMethodNotAllowed)
	case "GET":
		courseByClient(w, i)
	}
}

func ClientsFromCourse(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Methods", "GET")
	course := courseJSON{}
	if err := json.NewDecoder(r.Body).Decode(&course); err != nil {
		apihelper.MakeResponse(w, "Invalid inputs found", http.StatusUnprocessableEntity)
	}
	switch r.Method {
	case "OPTIONS":
		return
	case "GET":
	case "PUT":
		apihelper.MakeResponse(w, "Method Not Allowed", http.StatusMethodNotAllowed)
	case "POST":
		clientsFromCourse(w, course.Name)
	}
}

func ClientsFromClass(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Methods", "GET")
	class := classJSON{}
	if err := json.NewDecoder(r.Body).Decode(&class); err != nil {
		apihelper.MakeResponse(w, "Invalid inputs found", http.StatusUnprocessableEntity)
	}
	log.Println(class)
	switch r.Method {
	case "OPTIONS":
		return
	case "GET":
	case "PUT":
		apihelper.MakeResponse(w, "Method Not Allowed", http.StatusMethodNotAllowed)
	case "POST":
		clientsFromClass(w, class.ClassID)
	}
}

func courseByClient(w http.ResponseWriter, ID int) {
	c := relation.CoursesByClient(ID)
	e := json.NewEncoder(w)
	if len(*c) < 1 {
		e.Encode([]relation.Curso{})
		return
	}
	e.Encode(c)
}

func clientsFromCourse(w http.ResponseWriter, course string) {
	sc := relation.ClientsFromCourse(course)
	e := json.NewEncoder(w)
	if len(*sc) < 1 {
		e.Encode([]relation.ClientePf{})
		return
	}
	e.Encode(sc)
}

func clientsFromClass(w http.ResponseWriter, class int) {
	sc := relation.ClientsFromClass(class)
	e := json.NewEncoder(w)
	if len(*sc) < 1 {
		e.Encode([]relation.ClientePf{})
		return
	}
	e.Encode(sc)
}

func clients(w http.ResponseWriter) {
	sc := relation.ShowClients()
	e := json.NewEncoder(w)
	if len(*sc) < 1 {
		e.Encode([]relation.ClientePf{})
		return
	}
	e.Encode(sc)
}
