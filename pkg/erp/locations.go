package erp

import (
	"beltrano-api/pkg/apihelper"
	"beltrano-api/pkg/relation"
	"encoding/json"
	"net/http"
)

func Locations(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Methods", "GET")
	switch r.Method {
	case "OPTIONS":
		return
	case "PUT":
		//
	case "GET":
		cities(w)
	case "POST":
		apihelper.MakeResponse(w, "Method Not Allowed", http.StatusMethodNotAllowed)
	}
}

func cities(w http.ResponseWriter) {
	sc := relation.ShowCities()
	e := json.NewEncoder(w)
	e.Encode(sc)
}
