package relation

// Estado
type Estado struct {
	EstadoID   int    `gorm:"primary_key"`
	Uf         string `gorm:"size:2"`
	EstadoNome string `gorm:"size:20"`
}

//Cidade
type Cidade struct {
	CidadeID   int    `gorm:"primary_key"`
	EstadoID   int    `gorm:"foreign_key"`
	CidadeNome string `gorm:"size:60"`
}

func ShowCities() *[]Cidade {
	db := dbConn()
	defer db.Close()
	c := []Cidade{}
	db.Find(&c)
	return &c
}
