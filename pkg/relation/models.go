package relation

import (
	// importing postgres dialect for the models
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

type conn struct {
	Host, Port, User, DBName, Password string
	SSL                                bool
}
