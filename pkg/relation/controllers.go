package relation

import (
	"fmt"
	"os"

	"github.com/jinzhu/gorm"
)

func dbConn() *gorm.DB {
	c := conn{
		Host:     os.Getenv("db_host"),
		Port:     os.Getenv("db_port"),
		User:     os.Getenv("db_user"),
		DBName:   os.Getenv("db_name"),
		Password: os.Getenv("db_pass"),
		SSL:      false,
	}
	db, err := gorm.Open("postgres", fmt.Sprintf("host=%s port=%s user=%s dbname=%s sslmode=disable password=%s", c.Host, c.Port, c.User, c.DBName, c.Password))
	if err != nil {
		panic(err)
	}
	return db
}
