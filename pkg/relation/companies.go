package relation

//ClientePj
type ClientePj struct {
	ClienteID    int    `gorm:"primary_key"`
	CNPJ         string `gorm:"size:11"`
	Cliente      string `gorm:"size:200"`
	CidadeID     int    `gorm:"foreignkey:CidadeId"`
	Endereco     string `gorm:"size:255"`
	CEP          string `gorm:"size:9"`
	Telefone     string `gorm:"size:15"`
	Contato      string `gorm:"size:100"`
	CargoContato string `gorm:"size:50"`
}

type NewCompany struct {
	Cliente      string `gorm:"size:200"`
	Endereco     string `gorm:"size:255"`
	Contato      string `gorm:"size:100"`
	CargoContato string `gorm:"size:50"`
}

func ShowCompanies() *[]ClientePj {
	db := dbConn()
	defer db.Close()
	var count int
	db.Find(&[]ClientePf{}).Count(&count)
	c := make([]ClientePj, count)
	db.SingularTable(true)
	db.Find(&c)
	return &c
}

func CoursesByCompany(ID int) (c *[]Curso) {
	db := dbConn()
	defer db.Close()
	db.LogMode(true)
	c = &[]Curso{}
	db.Raw("select cursos.* from cursos left join turmas on cursos.curso_id=turmas.curso_id left join pedidos_detalhes detalhes on turmas.turma_id=detalhes.turma_id right join pedidos on detalhes.pedido_id=pedidos.pedido_id right join clientes_pj clientes on pedidos.cliente_id=clientes.cliente_id where clientes.cliente_id=? group by cursos.curso_id", ID).
		Scan(&c)
	return
}

func CompaniesFromCourse(course string) *[]NewCompany {
	if course == "" {
		return nil
	}
	db := dbConn()
	defer db.Close()
	db.SingularTable(true)
	db.LogMode(true)
	c := []NewCompany{}
	// select clientes.nome, cursos.curso_nome from clientes_pj clientes join pedidos
	// on pedidos.cliente_id=clientes.cliente_id join pedidos_detalhes detalhes on
	// detalhes.pedido_id=pedidos.pedido_id join turmas on turmas.turma_id=detalhes.turma_id
	// join cursos on turmas.curso_id=cursos.curso_id
	// Code to query to be improved
	db.Raw("select clientes.cliente, clientes.endereco, clientes.contato, clientes.cargo_contato from clientes_pj clientes left join pedidos on pedidos.cliente_id=clientes.cliente_id right join pedidos_detalhes detalhes on detalhes.pedido_id=pedidos.pedido_id right join turmas on turmas.turma_id=detalhes.turma_id right join cursos on turmas.curso_id=cursos.curso_id where cursos.curso_nome=? group by clientes.cliente, clientes.endereco, clientes.contato, clientes.cargo_contato", course).
		Scan(&c)
	return &c
}

func CompaniesFromClass(class int) *[]NewCompany {
	if class == 0 {
		return nil
	}
	db := dbConn()
	defer db.Close()
	db.SingularTable(true)
	db.LogMode(true)
	c := []NewCompany{}
	// select clientes.nome, cursos.curso_nome from clientes_pj clientes join pedidos
	// on pedidos.cliente_id=clientes.cliente_id join pedidos_detalhes detalhes on
	// detalhes.pedido_id=pedidos.pedido_id join turmas on turmas.turma_id=detalhes.turma_id
	// join cursos on turmas.curso_id=cursos.curso_id
	db.Raw("select clientes.cliente, clientes.endereco, clientes.contato, clientes.cargo_contato from clientes_pj clientes left join pedidos on pedidos.cliente_id=clientes.cliente_id right join pedidos_detalhes detalhes on detalhes.pedido_id=pedidos.pedido_id right join turmas on turmas.turma_id=detalhes.turma_id where turmas.turma_id=? group by clientes.cliente, clientes.endereco, clientes.contato, clientes.cargo_contato", class).
		Scan(&c)
	return &c
}

func (pj ClientePj) TableName() string {
	return "clientes_pj"
}
