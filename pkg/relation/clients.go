package relation

import (
	"log"
)

// ClientePf
type ClientePf struct {
	ClienteID int    `gorm:"primary_key" json:"clienteId"`
	CPF       string `gorm:"size:11" json:"cpf"`
	Nome      string `gorm:"size:200" json:"nome"`
	Sobrenome string `gorm:"size:200" json:"sobrenome"`
	Sexo      string `gorm:"size:1" json:"sexo"`
	CidadeID  int    `gorm:"foreignkey:CidadeId" json:"cidadeId"`
	Endereco  string `gorm:"size:255" json:"endereco"`
	CEP       string `gorm:"size:9" json:"cep"`
	Telefone  string `gorm:"size:15" json:"telefone"`
	Celular   string `gorm:"size:15" json:"celular"`
	Fax       string `gorm:"size:15" json:"fax"`
}

type NewClient struct {
	Nome      string `gorm:"size:200"`
	Sobrenome string `gorm:"size:200"`
	Endereco  string `gorm:"size:255"`
	Celular   string `gorm:"size:15"`
}

func Client(ID int) (c *ClientePf) {
	return
}

func CoursesByClient(ID int) (c *[]Curso) {
	db := dbConn()
	defer db.Close()
	db.LogMode(true)
	c = &[]Curso{}
	db.Raw("select cursos.* from cursos left join turmas on cursos.curso_id=turmas.curso_id left join pedidos_detalhes detalhes on turmas.turma_id=detalhes.turma_id right join pedidos on detalhes.pedido_id=pedidos.pedido_id right join clientes_pf clientes on pedidos.cliente_id=clientes.cliente_id where clientes.cliente_id=? group by cursos.curso_id", ID).
		Scan(&c)
	//db.Where("cliente_id = ?", ID).Find(&c)
	return
}

func ShowClients() *[]ClientePf {
	db := dbConn()
	defer db.Close()
	var count int
	db.Find(&[]ClientePf{}).Count(&count)
	c := make([]ClientePf, count)
	db.SingularTable(true)
	db.Find(&c)
	return &c
}

func ClientsFromCourse(course string) *[]NewClient {
	if course == "" {
		return nil
	}
	db := dbConn()
	defer db.Close()
	log.Println("clients from course", course)
	db.SingularTable(true)
	db.LogMode(true)
	c := []NewClient{}
	// select clientes.nome, cursos.curso_nome from clientes_pf clientes join pedidos
	// on pedidos.cliente_id=clientes.cliente_id join pedidos_detalhes detalhes on
	// detalhes.pedido_id=pedidos.pedido_id join turmas on turmas.turma_id=detalhes.turma_id
	// join cursos on turmas.curso_id=cursos.curso_id
	// Code to query to be improved
	db.Raw("select distinct clientes.nome, clientes.sobrenome, clientes.endereco, clientes.celular from clientes_pf clientes left join pedidos on pedidos.cliente_id=clientes.cliente_id right join pedidos_detalhes detalhes on detalhes.pedido_id=pedidos.pedido_id right join turmas on turmas.turma_id=detalhes.turma_id right join cursos on turmas.curso_id=cursos.curso_id where cursos.curso_nome=? group by clientes.nome, clientes.sobrenome, clientes.endereco, clientes.celular", course).
		Scan(&c)
	return &c
}

func ClientsFromClass(class int) *[]NewClient {
	if class == 0 {
		return nil
	}
	db := dbConn()
	defer db.Close()
	log.Println("clients from class", class)
	db.SingularTable(true)
	db.LogMode(true)
	c := []NewClient{}
	// select clientes.nome, cursos.curso_nome from clientes_pf clientes join pedidos
	// on pedidos.cliente_id=clientes.cliente_id join pedidos_detalhes detalhes on
	// detalhes.pedido_id=pedidos.pedido_id join turmas on turmas.turma_id=detalhes.turma_id
	// join cursos on turmas.curso_id=cursos.curso_id
	db.Raw("select distinct clientes.nome, clientes.sobrenome, clientes.endereco, clientes.celular from clientes_pf clientes left join pedidos on pedidos.cliente_id=clientes.cliente_id right join pedidos_detalhes detalhes on detalhes.pedido_id=pedidos.pedido_id right join turmas on turmas.turma_id=detalhes.turma_id where turmas.turma_id=? group by clientes.nome, clientes.sobrenome, clientes.endereco, clientes.celular", class).
		Scan(&c)
	return &c
}

func (pf ClientePf) TableName() string {
	return "clientes_pf"
}

func (nc NewClient) TableName() string {
	return "clientes_pf"
}
