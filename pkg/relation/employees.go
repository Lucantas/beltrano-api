package relation

import "time"

//Empregado
type Empregado struct {
	EmpregadoID     int    `gorm:"primary_key"`
	Sobrenome       string `gorm:"size:50"`
	Nome            string `gorm:"size:50"`
	CPF             string `gorm:"size:11"`
	Cargo           string `gorm:"size:50"`
	DataNasc        time.Time
	DataContratacao time.Time
	DataDemissao    time.Time
	Endereco        string
	CEP             string `gorm:"size:9"`
	TelRes          string `gorm:"size:15"`
	Ramal           string `gorm:"gorm:10"`
	Chefe           int
	CidadeID        int    `gorm:"foreignkey:CidadeId"`
	Comentarios     string `gorm:"size:244"`
	Departamento    string `gorm:"size:100"`
}

func ShowEmployees() *[]Empregado {
	db := dbConn()
	defer db.Close()
	e := []Empregado{}
	db.Find(&e)
	return &e
}
