package relation

type User struct {
	Name     string
	Username string
	Email    string
}
