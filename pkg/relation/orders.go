package relation

// Pedido
type Pedido struct {
	PedidoID   int `gorm:"primary_key"`
	ClienteID  int
	VendedorID int `gorm:"foreign_key"`
}

// PedidosDetalhes
type PedidosDetalhes struct {
	PedidoID      int `primary_gorm:"foreign_key"`
	ItemID        int `gorm:"primary_key"`
	Quantidade    int
	TurmaID       int `gorm:"foreign_key"`
	Desconto      float32
	TotalItem     float32
	PrecoUnitario int
}

type NewPedido struct {
	PedidoID           int `gorm:"primary_key"`
	TotalItem          float32
	PrecoUnitario      int
	EmpregadoNome      string
	EmpregadoSobrenome string
}

func ShowOrders() *[]Pedido {
	db := dbConn()
	defer db.Close()
	r := []Pedido{}
	db.Find(&r)
	return &r
}

func EmployeeOrders(employeeID int) *[]NewPedido {
	db := dbConn()
	defer db.Close()
	so := []NewPedido{}
	db.Raw("select distinct pedidos.pedido_id, detalhes.total_item, detalhes.preco_unitario, detalhes.quantidade, empregados.nome as empregado_nome, empregados.sobrenome as empregado_sobrenome from pedidos join empregados on pedidos.vendedor_id=empregados.empregado_id join pedidos_detalhes detalhes on pedidos.pedido_id=detalhes.pedido_id where empregados.empregado_id = ?", employeeID).Scan(&so)
	return &so
}
