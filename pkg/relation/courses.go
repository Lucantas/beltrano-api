package relation

import "time"

// Curso
type Curso struct {
	CursoID      int `gorm:"primary_key"`
	AutorID      int `gorm:"foreign_key"`
	CursoNome    string
	DuracaoTotal int
	DuracaoAula  int
	Vagas        int
	PrecoVaga    float32
}

//Turma
type Turma struct {
	TurmaID   int `gorm:"primary_key"`
	CursoID   int `gorm:"foreign_key"`
	DataTurma time.Time
}

func ShowCourses() *[]Curso {
	db := dbConn()
	defer db.Close()
	c := []Curso{}
	db.Find(&c)
	return &c
}

func ShowClasses() *[]Turma {
	db := dbConn()
	defer db.Close()
	t := []Turma{}
	db.Find(&t)
	return &t
}
