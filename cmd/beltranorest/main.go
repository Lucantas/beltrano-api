package main

import (
	"beltrano-api/pkg/apihelper"
	"beltrano-api/pkg/erp"
	"log"
	"net/http"
	"os"

	"github.com/gorilla/mux"
)

func main() {
	r := mux.NewRouter()
	// endpoints to get data
	r.HandleFunc("/", handleMethods)
	r.HandleFunc("/employees", erp.Employees).Methods("GET")
	r.HandleFunc("/employees/{id}/orders", erp.EmployeeOrders)
	// clients
	r.HandleFunc("/clients", erp.Clients)
	r.HandleFunc("/clients/{id}/courses", erp.CoursesFromClient)

	r.HandleFunc("/clients/class", erp.ClientsFromClass)
	// companies
	r.HandleFunc("/companies", erp.Companies)
	r.HandleFunc("/companies/course", erp.CompaniesFromCourse)
	r.HandleFunc("/companies/{id}/courses", erp.CoursesFromCompany)

	// locations
	r.HandleFunc("/locations", erp.Locations)
	//courses
	r.HandleFunc("/courses", erp.Courses)
	// classes
	r.HandleFunc("/classes", erp.Classes)
	// orders
	r.HandleFunc("/orders", erp.Orders)
	// endpoints to put data
	//r.HandleFunc("/employees", erp.SaveEmployee)
	//r.HandleFunc("/clients", erp.SaveClient)
	//r.HandleFunc("/companies", erp.SaveCompany)
	r.Use(apihelper.Middleware)
	port := ":" + os.Getenv("PORT")
	log.Fatal(http.ListenAndServe(port, r))
}

func handleMethods(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Hello World!"))
}
